/*
 * Hi there and welcome to this little coding kata. Here is what you should do in javascript:
 * NOTE: do the exercises step by step
 *
 * 1. There is a file on the webserver, named lorem.html. It contains a lot of (html) text. 
 *    Write a javascript function, which fetches the contents of this file asynchronously 
 *    from the server and replaces the contents of the <div class="container"> div of the website.
 * 
 * 2. There are lots of o's in the imported text. As the letter o is very important for this exercise, 
 *    we should highlight it. Highlight all the o's (upper- and lowercase) with my-blue background (see styles.css for 
 *    more information about that color) and white font color, a 30% bigger font size and add some 
 *    padding so that every o stands out.
 * 
 * 3. The letter 'r' is also very important. Highlight it in the same way (upper- and lowercase), but use the my-orange
 *    color this time.
 * 
 * 4. Instead of text with "meaning", the PO wants to have all the words which are placed in paragraph
 *    tags to be sorted in alphabetical order. Get rid of all the punctuation, just display the words
 *    in the right order. Example: <p>what a requirement</p> becomes <p>a requirement what</p>. 
 * 
 */

const container = document.getElementsByClassName('container')[0];

// 1. Fetch the data 
function fetchData() {
    fetch('lorem.html')
        .then(res => res.text())
        .then(data => container.innerHTML = data)
        .catch(err => console.log('ERROR: ', err));
}

// 2. & 3. Highlight important characters
function highlightImportantCharacters(text, char, style) {
    text.innerHTML = text.innerHTML.replace(new RegExp('(' + char + ')', 'gi'), '<span class="highlight ' + style + '">$1</span>');
}

// 4. Strip punctuation & sort paragraph content
function sortParagraphText() {
    document.querySelectorAll('p').forEach(p => {
        p.textContent = clearPunctuationAndSort(p.textContent);
        // reapply the highlights
        highlightImportantCharacters(p, 'o', 'my-blue');
        highlightImportantCharacters(p, 'r', 'my-orange');
    });
}

function clearPunctuationAndSort(text) {
     return text.replace(/[^A-Za-z0-9_]/g, ' ')
        .split(' ')
        .sort((a, b) => a.localeCompare(b))
        .join(' ');
}

// Just the presentational logic
function nextStep(element, step) {
    pageTransition(container)
        .then(() => {
            switch (step) {
                case 1:
                    fetchData();
                    setActiveStep(element);
                    break;
                case 2:
                    highlightImportantCharacters(container, 'o', 'my-blue');
                    setActiveStep(element);
                    break;
                case 3:
                    highlightImportantCharacters(container, 'r', 'my-orange');
                    setActiveStep(element);
                    break;
                case 4:
                    sortParagraphText();
                    setActiveStep(element);
                default:
                    break;
            }
        });
}

function setActiveStep(element) {
    element.parentNode.classList.remove('active');
    element.parentNode.nextElementSibling.classList.add('active');
}

function pageTransition(element) {
    return new Promise(resolve => {
        element.classList.add('pre-animation');
        setTimeout(() => {
            element.classList.remove('pre-animation');
            resolve();
        }, 500);
    })
};
